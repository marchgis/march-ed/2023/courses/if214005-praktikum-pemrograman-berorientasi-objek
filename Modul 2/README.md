# Challenge

1. Pilih 1 produk digital
    App:
        - Google Search, Youtube, Gmail, Tokopedia, Twitter, Shopee
        Facebook, Lazada, Tiktok, Instagram, Agoda, Traveloka, Ruangguru,
        Playstore, Netflix, Github, Kereta Api, Klik BCA, Jenius, Bank Jago, Zoom
    Game:
        - Valorant, Mobile Legend, Genshin Impact, PUPG, League of Legends,
        DOTA, Free Fire, King of Fighters, dsb.
2. Identifikasi dan ranking use case dari proses bisnisnya
    Proses bisnis: sekumpulan aktivitas dari sebuah organisasi / produk untuk
    mencapai tujuannya

    Contoh Gojek
    Use Case | Nilai Prioritas
    --- | ---
    User dapat memesan driver untuk mengantarkan ke tujuan | 10
    User dapat memilih tujuan antar | 9
    User dapat melihat posisi driver | 7
    User dapat melakukan pembayaran via Gopay | 8

    Contoh Mobile Legend
    Use Case | Nilai Prioritas
    --- | ---
    Hero dapat mengeksplorasi map | 10
    Hero dapat menyerang dan menghancurkan Hero player lain | 9
    Hero dapat menyerang dan menghancurkan Base Turret | 9

3. Mulai membuat coding menggunakan pemrograman berorientasi objek untuk use case 1
```java
class Player {
    String username;
    String id;
    Hero[] hero;
}

// Dagangan Hero nya ML
class Hero {
    String name;
    String[] role;
    int attack;
    int defense;
    int mana;
    int hp;
}

// Hero koleksinya Player
class PlayerHero {
    Hero hero;
    String skin;
}

class MatchPlayerHero {
    PlayerHero playerHero;
    Position position;

    void moveUp () {
        this.position.y - 1;
    }
}

class Position {
    int x;
    int y;
}

```

## Metrics
1. Menerapkan seluruh konsep pemrograman berorientasi objek dengan tepat ⭐⭐⭐⭐⭐
2. Menerapkan software engineering best practice ⭐⭐⭐⭐⭐
3. Kesesuaian proses bisnis terhadap objek ⭐⭐⭐⭐⭐
4. Jumlah proses bisnis yang dapat didemonstrasikan ⭐⭐⭐⭐

## Deliverable
1. Command Line Interface
2. Web Service - contoh Spring Boot
3. UI - contoh Unity / Unreal / C# / C++ / Typescript / Dart
