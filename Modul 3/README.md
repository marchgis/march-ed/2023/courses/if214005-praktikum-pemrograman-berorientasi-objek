# Modul 3 Pemrograman Berorientasi Objek

## Method

### Method Tanpa Nilai Return

```java
import java.util.ArrayList;

class Manusia {
    /**
     * Method tanpa nilai return = nilai returnnya void
     */
    void bilangTidak() {
        System.out.println("Tidaaak !");
        // Tidak ada yang direturn
    }
    
    /**
     * Method tanpa nilai return
     * dengan satu parameter bertipe String
     */
    void bilang(String kalimat) {
        System.out.println("Mau bilang " + kalimat);
    }
    
    /**
     * Method tanpa nilai return
     * dengan satu parameter array dari String
     */
    void bilangBanyak(String[] listKalimat) {
        
        // join() adalah Class Method dari Class String
        // untuk menggabungkan array dari String menjadi String
        String gabunganKalimat = String.join(",", listKalimat);
        
        System.out.println("Mau bilang banyak " + gabunganKalimat);
    }
    
     /**
     * Method tanpa nilai return
     * dengan dua parameter yaitu
     * ArrayList dari String, dan String
     * ArrayList berfungsi untuk mempermudah mengelola data berbentuk array
     * ArrayList perlu diimport di awal file dengan perintah import java.util.ArrayList;
     */
    void bilangBanyakDari(ArrayList<String> listKalimat, String tempat) {
        String gabunganKalimat = String.join(",", listKalimat);
        System.out.println("Dari " + tempat + ", Mau bilang banyak " + gabunganKalimat);
    }
}

public class DemoBerbagaiPenggunaanMethod {
    public static void main(String args[]) {
        
        Manusia upin = new Manusia();
        upin.bilangTidak();
        upin.bilang("Ipin ipin !");
        
        String[] banyakKalimat = {"Telpon berdering", "Kereta parkir di depan rumah", "Betul betul betul"};
        upin.bilangBanyak(banyakKalimat);
        
        ArrayList<String> banyakKalimat2 = new ArrayList<>();
        banyakKalimat2.add("Ada roket di angkasa");
        banyakKalimat2.add("Bola bowling berat sekali");
        upin.bilangBanyakDari(banyakKalimat2, "Stadion GBK");
    }
}

```

### Method Dengan Nilai Return

```java
import java.util.ArrayList;

class Ilmu {
    String judul;
    String sumber;
    String isi;
    
    public Ilmu (String judul, String sumber, String isi) {
        this.judul = judul;
        this.sumber = sumber;
        this.isi = isi;
    }
}

class Manusia {
    
    /**
     * Method dengan nilai Return
     * dimana nilai Return nya adalah int
     * nilai Returnnya dapat digunakan oleh Variable lainnya
     * Method di Private agar hanya bisa digunakan oleh Class ini
     */
    private int hitungPertambahan(int a, int b) {
        int jawaban = a + b;
        return jawaban;
    }
    
    void jawabPertambahan(int a, int b) {
        int hasilPertambahan = this.hitungPertambahan(a, b);
        System.out.println("Hasil pertambahan " + a + " dengan " + b + " adalah " + hasilPertambahan);
    }
    
    ArrayList<Ilmu> ilmu = new ArrayList<>();
    
    /**
     * Method dengan nilai Return
     * dimana nilai Return nya adalah Class Ilmu yang kita buat
     */
    private Ilmu tambahIlmu(String judul, String sumber, String isi) {
        Ilmu ilmuBaru = new Ilmu(judul, sumber, isi);
        ilmu.add(ilmuBaru);
        
        return ilmuBaru;
    }
    
    void pelajariIlmuBaru(String judul, String sumber, String isi) {
        
        Ilmu ilmuBaru = tambahIlmu(judul, sumber, isi);
        
        System.out.println("\nTelah mempelajari ilmu baru yaitu " + ilmuBaru.judul + " dari " + ilmuBaru.sumber + " yang isinya " + ilmuBaru.isi);
        System.out.println("Sehingga alhamdulillah sekarang punya " + this.ilmu.size() + " ilmu, yaitu:");
        
        for (Ilmu i : this.ilmu) {
 
            // Print all elements of ArrayList
            System.out.println("- " + i.judul);
        }
    }
    
}

public class DemoBerbagaiPenggunaanMethod {
    public static void main(String args[]) {
        
        Manusia ipin = new Manusia();
        ipin.jawabPertambahan(46, 90);
        
        ipin.pelajariIlmuBaru("Matematika Terapan Untuk Lingkungan", "Tok Dalang", "Cobalah ukur luas sungai itu, bisakah ? ya bisa");
        ipin.pelajariIlmuBaru("Kecerdasan Buatan, Buatankah ?", "Ibu Mei Mei", "Kecerdasan Buatan merupakan keilmuan yang menyenangkan sekaligus unik");
    }
}

```

## Referensi
- [Operasi String di Java - Javatpoint](https://www.javatpoint.com/java-string)
- [Operasi List di Java - Javatpoint](https://www.javatpoint.com/java-list)

## Challenge
- Terpakan berbagai penggunaan Method pada project produk digital yang sedang dibangun, demonstrasikan minimal dalam bentuk aplikasi Command Line Interface !
